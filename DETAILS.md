# Laravel Yajra Datatables
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-yajra-datatables.git`
2. Go inside the folder: `cd laravel-yajra-datatables`
3. Run `cp .env.example .env` and then add your desired database username & password
4. Run `php artisan migrate`
5. Run `php artisan tinker` then `factory(App\User::class, 100)->create();`
6. Run `php artisan serve`
7. Open your favorite browser: http://localhost:8000/users

### Screen shot

Laravel 7 Yajra Datatables

![Laravel 7 Yajra Datatables](img/yajra.png "Laravel 7 Yajra Datatables")
