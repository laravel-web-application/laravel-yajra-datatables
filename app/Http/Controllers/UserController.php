<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function add(Request $request)
    {
        $user = new User();
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->save();
//        Toastr::success('User added successfully :)','Success');
//        Toastr::success('User added successfully', 'Welcome Back', ["positionClass" => "toast-top-center", "progressBar" => "true"]);
//        return redirect('test', compact('user'));
//        return redirect('users')->with('status', 'Profile updated!');
        $notification = array(
            'message' => 'User created successfully!',
            'alert-type' => 'success'
        );

        return Redirect::to('/users')->with($notification);
    }
}
